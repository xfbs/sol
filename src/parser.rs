use crate::types::{BuiltinType, Field, Struct, StructDef, Type};
use pest::iterators::Pair;
#[cfg(test)]
use pest::Parser;
use pest_derive::Parser;
use std::collections::BTreeMap;

#[derive(Parser)]
#[grammar = "definition.pest"]
struct DefParser;

#[derive(thiserror::Error, Debug)]
pub enum ParseError {
    #[error("Wrong rule")]
    WrongRule,
}

macro_rules! check_rule {
    ( $pair:ident, $rule:expr ) => {
        if $pair.as_rule() != $rule {
            return Err(ParseError::WrongRule);
        }
    };
}

pub fn parse_type(_pair: Pair<Rule>) -> Result<Type, ParseError> {
    Ok(Type::Builtin(BuiltinType::String))
}

pub fn parse_field(pair: Pair<Rule>) -> Result<Field, ParseError> {
    check_rule!(pair, Rule::struct_field);
    let mut inner = pair.into_inner();
    let name = inner.next().ok_or(ParseError::WrongRule)?;
    check_rule!(name, Rule::identifier);
    let type_name = inner.next().ok_or(ParseError::WrongRule)?;
    check_rule!(type_name, Rule::type_def);
    let field_type = parse_type(type_name)?;

    Ok(Field {
        name: name.as_str().into(),
        field_type,
    })
}

pub fn parse_struct(pair: Pair<Rule>) -> Result<StructDef, ParseError> {
    check_rule!(pair, Rule::struct_def);
    let mut inner = pair.into_inner();
    let name = inner.next().ok_or(ParseError::WrongRule)?;
    check_rule!(name, Rule::identifier);

    let struct_fields = inner.next().ok_or(ParseError::WrongRule)?;
    check_rule!(struct_fields, Rule::struct_fields);
    let mut fields = BTreeMap::new();
    for struct_field in struct_fields.into_inner() {
        let field = parse_field(struct_field)?;
        fields.insert(field.name, field.field_type);
    }

    Ok(StructDef {
        name: name.as_str().into(),
        value: Struct { fields },
    })
}

#[test]
fn can_parse_struct() {
    // can parse struct with single field
    let def = "struct net.xfbs.File { path: path }";
    let mut parsed = DefParser::parse(Rule::struct_def, def).unwrap();
    let parsed = parse_struct(parsed.next().unwrap()).unwrap();
    assert_eq!(parsed.name, "net.xfbs.File");
}

#[test]
fn can_parse_struct_trailing_comma() {
    // can parse struct with single field
    let def = "struct net.xfbs.File { path: path, }";
    let _parsed = DefParser::parse(Rule::struct_def, def).unwrap();
}

#[test]
fn can_parse_struct_multiple_fields() {
    // can parse struct with multiple fields
    let struct_def = "struct net.xfbs.Test {
        name: string,
        optional: string?,
        tags: [string],
        tags_exact: [string; 15],
        tags_limited: [string; 7..10]
    }";
    let _parsed = DefParser::parse(Rule::struct_def, struct_def).unwrap();
}

#[test]
fn can_parse_root_object() {
    // can parse root object with single field
    let def = "object net.xfbs.File { path: path }";
    let _parsed = DefParser::parse(Rule::object_def, def).unwrap();
}

#[test]
fn can_parse_root_object_trailing_comma() {
    // can parse root object with single field
    let def = "object net.xfbs.File { path: path, }";
    let _parsed = DefParser::parse(Rule::object_def, def).unwrap();
}

#[test]
fn can_parse_dependent_object() {
    // can parse root object with single field
    let def = "object net.xfbs.Image: net.xfbs.File { created: date }";
    let _parsed = DefParser::parse(Rule::object_def, def).unwrap();
}

#[test]
fn can_parse_object_multiple_fields() {
    // can parse root object with single field
    let def = "object net.xfbs.Album {
        artist: string,
        year: integer,
        genre: string?,
        tags: [net.xfbs.Tag],
    }";
    let _parsed = DefParser::parse(Rule::object_def, def).unwrap();
}

#[test]
fn can_parse_simple_type() {
    let _parsed = DefParser::parse(Rule::type_def, "string").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "bool").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "path").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "hash").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "integer").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "number").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "date").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "net.xfbs.Hash").unwrap();
}

#[test]
fn can_parse_optional_type() {
    let _parsed = DefParser::parse(Rule::type_def, "option<string>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<bool>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<path>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<hash>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<integer>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<number>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<date>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "option<net.xfbs.Hash>").unwrap();
}

#[test]
fn can_parse_array_type() {
    let _parsed = DefParser::parse(Rule::type_def, "[string]").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "[bool]").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "[path]").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "[path; 5]").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "[path; 5..9]").unwrap();
}

#[test]
fn can_parse_map_type() {
    let _parsed = DefParser::parse(Rule::type_def, "map<string, string>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "map<string, bool>").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "map<path, date>").unwrap();
}

#[test]
fn can_parse_tuple_type() {
    let _parsed = DefParser::parse(Rule::type_def, "(string, string)").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "(bool)").unwrap();
    let _parsed = DefParser::parse(Rule::type_def, "(integer, integer, integer)").unwrap();
}
